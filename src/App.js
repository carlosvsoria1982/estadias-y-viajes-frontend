import React from 'react';
import './App.css';

import { Container } from 'react-bootstrap'
import About from '../src/components/About'
import Footer from '../src/components/Footer'
import Home from '../src/components/Home'
import Navbar1 from '../src/components/Navbar'
// import 'bootstrap/dist/css/bootstrap.min.css'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Searching from './components/Searching';
import Terms from '../src/components/Terms'
import Province from '../src/components/Province/Province'
import Pricing from '../src/components/pricing/Pricing'
import Propietary from '../src/components/Propietary/Propietary'
import Commercial from '../src/components/Commercial/Commercial'
import PageNotFound from '../src/components/Error/PageNotFound'
// import ListProduct from '../src/components/Commercial/ListProducts'
function App() {

  // class App extends Component {



  return (
    <Router>
      <Container>
        <div className="App" >
          <section className="App-header">
            <Navbar1 />
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/about" exact component={About} />
              <Route path="/search" component={Searching} />
              <Route path="/terms" component={Terms} />
              <Route path="/province/:id" component={Province} />
              <Route path="/pricing/" component={Pricing} />
              <Route path="/propietary/:id" component={Propietary} />
              <Route path="/pricing/" component={Pricing} />
              <Route path="/commercial/" component={Commercial} />
              {/* <Route path="/404" component={PageNotFound} /> */}
              <Route path="*" component={PageNotFound}/>
            </Switch>
            <Footer />

          </section>

        </div>
      </Container>
    </Router>
  );
}


export default App;
