import React from 'react';
import { MapContainer, TileLayer, Marker, Popup, Polyline } from 'react-leaflet'
import { Col, Row, Container, Carousel } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { AccessAlarm, LocationOn, WhatsApp, Email, Phone } from '@material-ui/icons'
import { Link } from 'react-router-dom'
// import { faComment, faAt, faPhoneAlt, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
// import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom'
import '../Commercial/Style.css'
import SanJuan from '../Data/San Juan.json';
import { IMAGE_URL, BASE_URL, ENV } from '../../Enviroment/enviroment'
import { green, blue } from '@material-ui/core/colors';
function Show({ data }) {
    if (ENV === 'development') {
        // console.log("data: ",data)
    }
    var mailTo = `mailto:example@${data.email}`
    var images = data.images
    // const path = `propietary/${data.propietary.urlname}`
    const geo = [data.geoLocation.lat, data.geoLocation.lon]
    const polygon = SanJuan.features[0].geometry.coordinates[0]

    const WhatsAppUrl = `https://api.whatsapp.com/send?phone=${data.phoneNumber}&text=Hola! Te escribo por tu comercio ${data.commercialName} en ${BASE_URL}: `
    if (ENV === 'development') {
        // console.log(WhatsAppUrl)
    }

    var arreglo = []

    for (let index = 0; index < polygon.length; index++) {
        const a = polygon[index][0]
        const b = polygon[index][1]
        arreglo[index] = [b, a];
    }
    //   console.log(arreglo)
    const limeOptions = { color: 'lime' }
    // const rectangle = [
    //     [51.49, -0.08],
    //     [51.5, -0.06],
    // ]

    return (
        <div>
            <Container>
                <Row>
                    <Col className="align-left" lg={4}>
                        <h2>{data.commercialName}</h2>
                    </Col>
                    <Col className="align-right" lg={8}>
                        <Row >
                            <Col>
                                <h2>{data.province.provinceName}</h2>
                            </Col>

                        </Row>
                        <Row >
                            <Col>
                                {/* <h2>{data.location.locationName}</h2> */}
                            </Col>

                        </Row>

                    </Col>

                </Row>

                <Row>
                    <Col>
                        <h4 className="align-left" >{data.commercialSubType}</h4>
                    </Col>

                </Row>

                <Row>
                    <Col lg={5}>
                        <Row>
                            <h5>{data.commercialMainText}</h5>
                        </Row>
                        <Row>
                            <h5><LocationOn /> {data.address}</h5>
                        </Row>
                        <Row>
                            <h5 style={{ color: 'white' }} href={mailTo} ><Email /> {data.email}</h5>
                        </Row>
                        <Row>

                            <Link to={{ pathname: WhatsAppUrl }} target="_blank">
                                <h5 style={{ color: 'white' }}> <WhatsApp style={{ color: green[500] }} />&nbsp;Enviar mensaje </h5>

                            </Link>

                        </Row>
                        <Row>
                            <h5><a href="callto://+5492646702524" style={{ color: 'white' }}><Phone style={{ color: blue[500] }} /> {data.phoneNumber}</a></h5>
                        </Row>
                        <Row>
                            <h5><AccessAlarm /> {data.hoursOpen}</h5>
                        </Row>

                        <Row>

                        </Row>
                    </Col>
                    <Col >

                        <Carousel>
                            {
                                images &&
                                images.map((imagenes, index) => {
                                    return (
                                        <Carousel.Item key={index}>
                                            <img
                                                className="d-block w-100"
                                                src={IMAGE_URL + imagenes.imageUrl}
                                            />
                                            <Carousel.Caption>
                                                <h4 className="text-carrousel">{imagenes.imageMainText}</h4>
                                                <p className="text-carrousel">{imagenes.imageSubText}</p>
                                            </Carousel.Caption>
                                        </Carousel.Item>
                                    )
                                })
                            }
                        </Carousel>

                    </Col>


                    {/* <h5 >{data.phoneNumber}</h5>
                        <h5 >{data.hoursOpen}</h5> */}
                </Row>



                <Row>
                    <Col lg={4}>
                        <div>
                            <MapContainer center={geo} zoom={16} scrollWheelZoom={true}>
                                <TileLayer
                                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                />
                                <Marker position={geo}>
                                    <Popup>
                                        {data.commercialName}
                                    </Popup>

                                </Marker>
                                <Polyline pathOptions={limeOptions} positions={arreglo} >
                                    <Popup>{data.province.provinceName}</Popup>
                                </Polyline>
                            </MapContainer>
                            </div>
                        </Col>




                </Row>



            </Container>
            <div>
                
            </div>

        </div>
    );
}

export default Show;