import React, { useState, useEffect } from 'react';
import { BASE_URL, ENV } from '../../Enviroment/enviroment'
import Show from '../Commercial/Show'
import SmallCard from '../Province/SmallCard'

function Commercial() {
    const fullPathName = window.location.pathname
    const actualPathName = fullPathName.split('/')
    const urlCommercialName = actualPathName[2]
    const [search, updateSearch] = useState("")
    const [search1, updateSearch1] = useState("")
    const filter = urlCommercialName
    const nameSearch = "commercial"
    useEffect(function () {
        if (ENV === 'development') {
            console.log("actualizando....")
        }
        if (search1 !== 'null')
        {
        const baseURL = `${BASE_URL}/v1/${nameSearch}/get`
        var arr = {
            typeFilter: "single",
            collection: "commercial",
            data: {
                urlCommercialName: filter
            },

        }
        const newUrl = `${baseURL}`
        fetch(newUrl,
            {
                method: "POST",
                body: JSON.stringify(arr),
                headers:
                {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(res => res.json())
            .then(response => {
            var commercial = response.commercial[0]
            if (ENV === 'development') {
                console.log("response: ", response)
                console.log("commercial: ", commercial)
            }
            var temp = commercial.stays;
            var comercio = commercial;
            var paraMostrar = [];
            delete comercio["stays"];
            temp.map((stays) => {
            var temporal = {};
            var stays1 = { stays: stays };
            temporal = { ...temporal, ...comercio };
            temporal = { ...temporal, ...stays1 };
            // console.log(temporal);
            paraMostrar.push(temporal);
            });
            if (ENV === 'development') {
                console.log(paraMostrar);
            }
            updateSearch1(paraMostrar)
            updateSearch(commercial)
            })
        }
    }, [])
    return (
        <div>
            <div>
            {
                search &&
                <Show key ={1}
                data={search} />
                
            }
            </div>
            <div>
                 {
                    search1 &&
                    search1.map((comercial, index) =>
                        <SmallCard
                            key={comercial.stays._id}
                            data={comercial}
                        />
                    )

                } 

            </div>
        </div>
    );

}
export default Commercial;