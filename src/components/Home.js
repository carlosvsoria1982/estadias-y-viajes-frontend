import React from 'react'
import { Container } from 'react-bootstrap'
import Landing from '../components/Landing'
import Searching from '../components/Searching'

function Home() {
    return (
        
        <div className="App" >
            <section className="App-header">
                <Container>
                    {/* <Switch> */}
                    <Landing />
                    {/* <ButtonSearch /> */}
                    
                    <Searching />

                    {/* </Switch> */}

                    <br />
                </Container>

                </section>
                </div>
                
    )

}
export default Home;