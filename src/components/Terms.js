import React from 'react'

export default function Terms(){
    return (
        <div>

            <h1>
                Términos y Condiciones
            </h1>
            <h2>
                estadiasyviajes.com es una plataforma que busca acercar a los comerciantes con los clientes finales, por lo tanto no se responsabiliza por el incumplimiento de los acuerdos particulares que lleguen.
            </h2>
        </div>
    )

}