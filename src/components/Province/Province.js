import React, { useState, useEffect } from 'react'
import { ENV } from '../../Enviroment/enviroment'

import Show from '../Province/Show'
import ListProducts from '../Generals/ListProducts'
import ButtonSearch from './ButtonSearch'
function Province({ history }) {
  const fullPathName = window.location.pathname
  const actualPathName = fullPathName.split('/')
  const urlProvinceName = actualPathName[2]
  // console.log(fullPathName, actualPathName, urlProvinceName)
  if (ENV === 'development') {
    console.log("province", window.location.origin)
  }

  const [province, updateProvince] = useState([])
  // const [toShow, updateToShow] = useState([])
  // const [count, setCount] = useState(0);
  const buttons = [
    { name: 'Estadías', value: '1' },
    { name: 'Experiencias', value: '2' },
    { name: 'Gastronomía', value: '3' },
    { name: 'Lugares', value: '4' },
    { name: 'Localidades', value: '5' },
  ];
  var arr = {
    collection: "province",
    data: {
      "urlName": urlProvinceName
    },
    method: "POST"
  }
  // var filter = { 'urlName': `${urlProvinceName}` }
  // const mainSearch = { 'nameSearch': 'province' }
  var data = []
  useEffect(function () {
    
    ListProducts({ arr })
      .then(response => {
        data = response.province[0]
        updateProvince(data)
      })
  }, [])
  // console.log(count)
  return (
    <div>
      {
        province &&
        <Show
          key={province.urlName}
          url={province.urlName}
          Name={province.provinceName}
          mainText={province.mainTextProvince}
          images={province.images}
          id={province._id}
        />
      }
      <br>
      </br>
      {
        province &&
        <ButtonSearch
          buttons={buttons}
          url={urlProvinceName}
          id={province._id}
        />
      }
      {/* {
        province &&
        console.log(province.urlName)
      } */}
    </div>
  );
}

export default Province;