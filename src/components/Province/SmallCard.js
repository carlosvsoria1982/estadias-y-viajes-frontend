import React from 'react';
import '../Province/Style.css'

import { Card, Col, Row, Carousel} from 'react-bootstrap'
import {Link } from 'react-router-dom'

import { AcUnit, Hotel, Kitchen, FreeBreakfast, Pool, OutdoorGrill, ArrowUpward, ArrowDownward, ExitToApp, LocalDining, Deck, LiveTv, LocalParking, LocalLaundryService, Facebook, Instagram, WhatsApp, AlternateEmail, RecordVoiceOver, Flare, Restaurant, Info, Person, Wifi, Weekend} from '@material-ui/icons'
import {green} from '@material-ui/core/colors';
import { IMAGE_URL, BASE_URL} from '../../Enviroment/enviroment'
function SmallCard({data}) {
    const fullPathName = window.location.pathname
    const actualPathName = fullPathName.split('/')
    const urlCommercialName = actualPathName[1]
    // console.log(urlCommercialName)
    // console.log(data)
    // const path = window.location.pathname
    const baseURL = `https://estadiasyviajes.com`
    const WhatsAppUrl = `https://api.whatsapp.com/send?phone=${data.phoneNumber}&text=Hola! Te escribo por tu aviso en ${BASE_URL}: ${data.stays.stayName}`
    return (
        <div>

            <Card style={{ color: "#000", width: 'auto', margin: 5 }}>
            <Card.Header>{data.stays.stayName}</Card.Header>
                <Row>
                    <Col sm={5}>
                    <Carousel>
            {
                data.stays.images &&
                data.stays.images.map((imagenes, index) => {
                    return (
                        <Carousel.Item>
                            <img
                                className="d-block w-100"
                                src={IMAGE_URL+imagenes.imageUrl}
                            />
                            <Carousel.Caption>
                                <h4 className="text-carrousel">{imagenes.imageMainText}</h4>
                                <p className="text-carrousel">{imagenes.imageSubText}</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    )
                })
            }
        </Carousel>
                        {/* <Card.Img variant="top" src="http://localhost:8081/public/image-1612755943764.jpg" /> */}
                    </Col>

                    <Col sm={4}>
                        <div className="alineacion-justify">
                            <h6>{data.stays.mainText}</h6>
                        </div>
                            
                            <Row>
                               
                         <ul>
                            {
                                (data.stays.wifi === true) &&
                                <li>
                                    <Wifi/> Wifi
                                </li>
                            }

                            {
                                (data.stays.airConditioning === true) &&                                
                                <li>
                                <AcUnit/> Aire Ac.
                                </li>

                            }
                                
                            {
                                (data.stays.smartTV === true) &&
                                <li>
                                <LiveTv/> SmartTv
                                </li>
                            }
                            {
                                (data.stays.breakfast === true) &&
                                <li>
                                <FreeBreakfast/> Desayuno
                                </li>
                            }
                            {
                                (data.stays.pool === true) &&
                                <li>
                                    <Pool/> Pileta
                                </li>
                            }
                            
                            {
                                (data.stays.deck === true) &&
                                <li>
                                    <Deck/> Quincho
                                </li>
                            }
                            
                            {
                                (data.stays.beds > 0) &&
                                <li>
                                <Hotel/> Camas
                                </li>
                            }
                            
                            {
                                (data.stays.kitchen === true) &&
                                <li>
                                    <Flare/> Cocina
                                </li>
                            }
                            {
                                (data.stays.kitchen === true) &&
                                <li>
                                    <Kitchen/> Freezer
                                </li>
                            }
                            
                            {
                                (data.stays.grills === true) &&
                                <li>
                                <OutdoorGrill/> Parrillero
                                </li>
                            }

                            {
                                (data.stays.plate === true) &&
                                <li>
                                    <Restaurant/> Vajilla
                                </li>
                            }

                            {
                                (data.stays.laundryService === true) &&
                                <li>
                                    <LocalLaundryService/> Lavarropas
                                </li>
                            }
                            
                            {
                                (data.stays.parking===true) &&
                                <li>
                                <LocalParking/> Estacionamiento
                                </li>
                            }
                            {
                                (data.stays.armchair===true) &&
                                <li>
                                <Weekend/> Sofá
                                </li>
                            }
                        </ul>
                               

                            </Row>
                    </Col>
                    <Col sm={3}>
                        <h5><Person/> x {data.stays.ocupancy}</h5>
                        <h5>CheckIn: {data.stays.checkIn} hs</h5>
                        <h5>CheckOut: {data.stays.checkOut} hs</h5>
                        

                        {
                        urlCommercialName!="commercial" &&
                        <Link to={{ pathname: WhatsAppUrl}} target="_blank">
                        <h5>
                        <WhatsApp style={{ color: green[500] }} />
                        &nbsp;&nbsp;&nbsp;{data.phoneNumber}
                        </h5>
                        </Link>
                        }
                        

                        
                            {
                                urlCommercialName!="commercial" &&
                                <Link to={{ pathname: `/commercial/${data.urlCommercialName}`}}>
                                <h5><Info/> Más info...</h5>
                                </Link>
                            }
                                
                        

                    </Col>
                    
                    

                </Row>
                
                
                {/* <Card.Footer className="text-muted">Kupal Departamentos</Card.Footer>     */}
            </Card>

        </div>
    );
}

export default SmallCard;