import React from 'react';
import { Col, Row, Carousel } from 'react-bootstrap'
import '../Province/Style.css'
import { IMAGE_URL} from '../../Enviroment/enviroment'
function Show({ url, Name, mainText, images, id }) {
    return (
        <div>
            <Row>
                <Col>
                    <h3 >
                        {Name}
                    </h3>
                </Col>

            </Row>
            <Row>
                <Col lg={5}>
                    <h5>
                        {mainText}
                    </h5>
                </Col>
                <Col>
                    <Carousel>
                        {
                            images &&
                            images.map((imagenes, index) => {
                                return (
                                    <Carousel.Item>
                                        <img
                                            className="d-block w-100"
                                            src={IMAGE_URL+imagenes.imageUrl}
                                        />
                                        <Carousel.Caption>
                                            <h4 className="text-carrousel">{imagenes.imageMainText}</h4>
                                            <p className="text-carrousel">{imagenes.imageSubText}</p>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                )
                            })
                        }
                    </Carousel>
                </Col>
            </Row>

            {/* <div>
                {console.log(images)}
            </div> */}
        </div>
    );

}

export default Show;