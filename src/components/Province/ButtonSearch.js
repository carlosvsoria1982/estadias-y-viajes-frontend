import React, { useState, useEffect } from "react";
import { Button } from 'react-bootstrap'
import ListProducts from '../Generals/ListProducts'
import SmallCard from '../Province/SmallCard'
import { ENV } from '../../Enviroment/enviroment'
function ButtonSearch({ buttons, url, id }) {

  const [count, setCount] = useState('');
  const [commercial, setCommercial] = useState([])

  var arr = {
    collection: "provinces",
    data: {
      "urlName": url
    },
    method: "POST"
  }

  if (ENV === 'development') {
    console.log("arr ButtonSearch", arr)
  }
  useEffect(function () {
    if (ENV === 'development')
    {
      console.log("useEffect: ", count)
    }
    if (count !== 'null') {
      if (ENV === 'development')
      {
        console.log("entrendo al if")
      }
  
      switch (count) {
        case (1):
          {
            arr.collection = "commercial"
            arr.typeFilter = "multi"
            arr.nameFilter = "stays"
            // arr.data.province = "sanjuan"
            if (ENV === 'development')
            {
              console.log("case 1")
              console.log(arr)
            }
            
            ListProducts({ arr })
              .then(response => {
                if (ENV === 'development')
                {
                  console.log("esta es la respuesta...")
                }
                
                var temp = response
                // temp.map(comercial => {
                //   delete comercial.images
                //   delete comercial.experiences
                // })
                if (ENV === 'development')
                {
                  console.log(temp.commercial)
                }
                
                setCommercial(temp.commercial)
              })
            break;
          }
        case (2):
          {
            if (ENV === 'development')
            {
              console.log("case 2")
            }
            // mainSearch = { 'nameSearch': 'commercial' }
            // filter = { 'province': 'sanjuan', 'commercialType': 'experiences' }
            break;
          }
        case (3):
          {
            // mainSearch = { 'nameSearch': 'commercial' }
            // filter = { 'filter': 'stays' }
            if (ENV === 'development')
            {
              console.log("case 3")
            }

            break;
          }
        case (4):
          {
            if (ENV === 'development')
            {
              console.log("case 4")
            }
            // mainSearch = { 'nameSearch': 'commercial' }
            // filter = { 'stayType': 'Departamento Amoblado' }
            // console.log(mainSearch, filter)
            break;
          }




      }



    }
  }, [count])
  //  function holas(value){
  //   setCount(value)
  //   // console.log("holas")
  //  }  


  return (
    <>
      <Button className="texto" variant="secondary" size="lg" onClick={() => { setCount(1) }}>{buttons[0].name}</Button>
      <Button className="texto" variant="secondary" size="lg" onClick={() => { setCount(2) }}>{buttons[1].name}</Button>
      <Button className="texto" variant="secondary" size="lg" onClick={() => { setCount(3) }}>{buttons[2].name}</Button>
      {/* <Button className="texto" variant="secondary" size="lg" onClick={() => { setCount(4) }}>{buttons[3].name}</Button>
      <Button className="texto" variant="secondary" size="lg" onClick={() => { setCount(5) }}>{buttons[4].name}</Button> */}
      {/* <div>
        {
          count &&
          <h4>{count}</h4>
        }
      </div> */}

      <div>

        {
          commercial &&
          commercial.map((comercial, index) =>

            <SmallCard
              key={index}
              data={comercial}

            />
          )
        }


      </div>
    </>


  )
};


export default ButtonSearch;