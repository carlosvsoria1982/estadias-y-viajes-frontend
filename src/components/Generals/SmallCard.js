import React, { useState } from "react";
import { Button, Card, Nav, Form, FormControl, Footer, Col, Row, Carousel, Modal, Image } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../Commercial/Style.css'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { faBed, faSnowflake, faDoorOpen, faWifi, faParking, faUtensils } from '@fortawesome/free-solid-svg-icons'
import {Link } from 'react-router-dom'
function SmallCard({ name, type, image, mainText, beds, rooms, kitchen, wifi, parking, airConditioning }) {
    const [show, setShow] = useState(false);

    return (
        <div>
            <Card style={{ color: "#000", width: 250, height: 'auto', margin: 10 }}>
                <Card.Header>{type}</Card.Header>
                <Card.Img variant="top" src={image[0].imageUrl} />
                <Card.Body>
                    <Card.Title >{name}</Card.Title>
                    <Card.Text> <FontAwesomeIcon icon={faBed} /> x {beds}
                    </Card.Text>
                    <Button variant="primary" onClick={() => setShow(true)}>+info!</Button>
                </Card.Body>
            </Card>
            <Modal
                show={show}
                size="lg"
                onHide={() => setShow(false)}
                dialogClassName="modal-90w"
                aria-labelledby="example-custom-modal-styling-title"

            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-custom-modal-styling-title">
                        {name}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Carousel>
                            {
                                image.map((imagenes, index) => {
                                    return (

                                        <Carousel.Item key={index}>
                                            <img
                                                className="d-block w-100"
                                                src={imagenes.imageUrl}
                                            />
                                            <Carousel.Caption>
                                                <h4 className="text-carrousel">{imagenes.imageMainText}</h4>
                                                <p className="text-carrousel">{imagenes.imageSubText}</p>
                                            </Carousel.Caption>
                                        </Carousel.Item>

                                    )
                                })
                            }
                        </Carousel>
                    <p>
                        {mainText}
                    </p>
                    <p>
                        La propiedad tiene los siguientes servicios:
                    </p>                    
                    <p>
                    <Row className="text-center font-weight-bold" >
                        <Col>
                            <FontAwesomeIcon icon={faBed} title="Camas"/> x {beds}
                        </Col>
                        
                            {
                            (kitchen == true) &&
                            <Col>
                                <FontAwesomeIcon icon={faUtensils} title="Cocina"/>
                            </Col>
                             }
                        
                        
                            {
                            (airConditioning == true) &&
                            <Col>
                                <FontAwesomeIcon icon={faSnowflake} title="Aire Acondicionado"/>
                                </Col>
                            }
                        
                        
                            {
                            (rooms > 0) &&
                            <Col>
                                <FontAwesomeIcon icon={faDoorOpen} title="Habitaciones"/> x {rooms}
                            </Col>
                            }
                        
                            {
                            
                            (wifi == true) &&
                            <Col>    
                                <FontAwesomeIcon icon={faWifi} title="WiFi"/>
                            </Col>
                            }
                        
                            {
                            (parking == true) &&
                            <Col>
                                <FontAwesomeIcon icon={faParking} title="Estacionamiento"/>
                            </Col>
                            }
                        
                    </Row>
                    
                    
                    
                    
                    


                </p>
                <h5>Consulta disponibilidad al Administrador</h5>
                <div>

                </div>
                </Modal.Body>
                
            </Modal>
        </div>


    )
};

export default SmallCard;