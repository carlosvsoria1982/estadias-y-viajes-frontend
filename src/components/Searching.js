import React from 'react'
import ResultSearch from '../components/ResultSearch'
import SearchProvince from '../components/SearchProvince'
import { useEffect, useState } from 'react';
import { Row, Container } from 'react-bootstrap'
import { ENV } from '../Enviroment/enviroment'

function Searching() {
  const [search, updateSearch] = useState([])
  if (ENV === 'development') {
    console.log(process.env.PUBLIC_URL)
  }
  useEffect(function () {
    SearchProvince().then(search => updateSearch(search))

  }, [])
  return (
    
      <div>
        <Container>
          <Row className="justify-content-center" sm={4}>
            
            {search.map(({provinceName,urlName,mainImage, subtittleProvince,_id,stays, places, experiences  }) =>
            <ResultSearch 
                imageMainText={provinceName}
                images={mainImage}
                urlName={urlName}
                imageSubText={subtittleProvince}
                staysCount={stays}
                placesCount={places}
                experiencesCount={experiences}
                id={_id}
                key={_id} />
            )}


          </Row>
        </Container>

      </div>
    
  )
}
export default Searching