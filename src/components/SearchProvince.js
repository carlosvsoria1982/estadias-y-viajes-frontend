// import React from 'react'
import { BASE_URL, ENV } from '../Enviroment/enviroment'

const baseURL = `${BASE_URL}/v1/province/get`
function SearchProvince() {

    return  fetch(baseURL,
        {
            method: "POST",
            body: JSON.stringify({}),
            headers:
        {
            "Content-type": "application/json; charset=UTF-8"
        }
        })
        .then(res => res.json())
        .then(response => {
            const { province } = response
            // console.log("nombre de provincia")
            // console.log(province)
            const value= province.map(province => {
                const {provinceName, urlName, _id, subtittleProvince, stays, experiences, places, images} = province
                const mainImage=images[0]
                return {provinceName, urlName, _id, subtittleProvince, stays, experiences, places, mainImage}
            })
            if (ENV === 'development')
            {
                console.log(value)
            }
            return value
        },
        err => {
           console.log("error")
        })
}
export default SearchProvince;