import React from 'react';

import '../App.css';
import imagen1 from '../Portada.jpg'

function Landing() {
    
    return (
        <div>
            

            <div className="row">
                {/* <img className="caption" src={imagen1} style={{ height: 1920, filter: 'grayscale(100%)' }} /> */}
                <h1>
                        ¡¿Querés conocer nuevos lugares y vivir nuevas experiencias?!
                </h1>
                <img src={imagen1} className="img" />

            </div>
            <div className="info-footer">
                <h2>Tu próximo viaje te esta esperando, elegí la provincia de tu destino</h2>
            </div>
        </div>
    )
}

export default Landing;