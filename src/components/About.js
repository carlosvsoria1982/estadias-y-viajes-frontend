import React from 'react'
import { Container } from 'react-bootstrap';
function About() {
    return (
        <Container>
            <div>

                <h1>Acerca de</h1>
                <div>
                    <h2>Que es estadiasyviajes.com?</h2>
                    <p>
                        Es una plataforma online que permite encontrar rápidamente hospedaje, experiencias y lugares para conocer.
                    </p>
                    <h2>Como lo logra?</h2>
                    <p>
                        Paso 1- El propietario se contacta con nuestro equipo de soporte para crear el aviso.
                    </p>
                    <p> 
                        Paso 2- Nuestros profesionales en Diseño y Turismo lo asesoran para que el aviso se vea excelente.
                    </p>
                    <p>
                        Paso 3- Con el aviso ya creado, resta elegir el tipo de aviso; Gratis!, intermedio, avanzado.
                    </p>
                    <p>
                        Paso 4- Te pasamos un link con el aviso y tu perfil cargado. A partir de ahora los clientes que vean tu aviso te contactarán directamente a tu teléfono celular.            
                    </p>
                    <h2>Motivación</h2>
                    <p>
                        Todos hemos padecido lo dificultoso que resulta encontrar información sobre hospedajes, lugares y experiencias; ya sea al momento de planificar un viajes o cuando uno ya se encuentra en el destino. La realidad es que NO TODO lo que un turista busca se encuentra con unos pocos clicks en Internet, y menos en un solo lugar.                       
                    </p>    
                    <p>
                        Muchos propietarios, no siempre tienen la habilidad de "Publicitar" su producto/servicio con toda la información necesaria en internet.                    </p>
                    <p>
                        Esta pandemia que nos afectó en el 2020, trajo sobre el tablero los nuevos paradigmas del Turismo, el cual ya no será el mismo, por ello nos preguntamos como podemos ayudar a levantar una industria muy golpeada.
                    </p>
                    <p>
                        Nos pusimos a pensar y a trabajar en como solucionar nuestros problemas en común, y el resultado fue nuestra plataforma; la cual intenta ser intuitiva y fácil de utilizar tanto para propietarios como viajeros tengan resultados rápido y eficientes.
                    </p>
                    <h2>
                        Quienes somos..
                    </h2>
                    <p>
                        Somos un grupo de emprendedores de diferentes rubros que buscamos potenciar no solo nuestros emprendimientos particulares, sino aquellos de la comunidad que históricamente, por diversos factores,no han podido ser visibilizados en la web.
                    </p>
                </div>
            </div>
        </Container>
    )
}
export default About;