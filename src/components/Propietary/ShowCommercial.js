import React from 'react';

import { Button, Card, Col, Row } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../Commercial/Style.css'

// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faCoffee } from '@fortawesome/free-solid-svg-icons'
// import { faBed, faSnowflake, faDoorOpen, faWifi, faParking, faUtensils } from '@fortawesome/free-solid-svg-icons'

import {Link } from 'react-router-dom'



function  ShowCommercial({title, picture, link, subTitle}){
    return (
        <>
        <Row>

        <Col lg={4}>
            <Card style={{ color: "#000", width: 250, height: 'auto', margin: 10 }}>
                <Card.Header>
                    {title}
                </Card.Header>
                <Card.Img variant="top" src={picture[0].imageUrl} />
                
                <Card.Text>
                    {subTitle}
                </Card.Text>
                <Link to={{ pathname: `/commercial/${link}`}}>
                    <Button variant="primary">+info!</Button>
                </Link>
                
            </Card>
        </Col>
        </Row>
        </>            
   
    );
}

export default ShowCommercial;