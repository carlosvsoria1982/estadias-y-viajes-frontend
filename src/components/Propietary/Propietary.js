import React from 'react';
import { useState, useEffect } from 'react'

// import { Col, Row, Container, Carousel, Spinner } from 'react-bootstrap'
import ListProducts from '../Generals/ListProducts'
import Show from '../Propietary/Show'
import ShowCommercial from '../Propietary/ShowCommercial'
import { IMAGE_URL, BASE_URL, ENV} from '../../Enviroment/enviroment'

function Propietary () {    

  const fullPathName = window.location.pathname
  const actualPathName = fullPathName.split('/')
  const urlPropietaryName = actualPathName[2]
  if (ENV === 'development'){
    console.log(fullPathName)
  }
  
    const [search1, updateSearch1] = useState([])
    const [commercial, updateCommercial] = useState([])

    // console.log("--------")
    // console.log(search1)
    // console.log("--------")
    var nameFilter = "urlname"
    var filter = urlPropietaryName
    var nameSearch = "propietary"
    var data =""
    useEffect(function () {
      // console.log("--------")
      // console.log(search1)
      // console.log("--------")
      ListProducts({nameFilter, filter, nameSearch})
      .then( result => {
        // console.log(result)
        updateSearch1(result)
        data=result
        const data1= data[0]._id
        //  console.log(data1)
        
        nameFilter = "propietary"
        nameSearch="commercial"
        filter=data1
        ListProducts({nameFilter, filter, nameSearch})
        .then(response =>{
          updateCommercial(response)
        })
        
        
      })


      
        
      


    }, [])
    return (
      <div>
  
            {
              search1 &&
              search1.map(({active,
                bio,
                email,
                experiences,
                images,
                languages,
                phoneNumber,
                propietaryName,
                proptietaryText,
                social,
                stays,
                urlname,
                webSite,
                geoLocation}, index) =>

               <Show 
               key={index}
               active= {active}
               bio={bio}
               email={email}
               experiences={experiences}
               images={images}
               languages={languages}
               phoneNumber={phoneNumber}
               propietaryName={propietaryName}
               proptietaryText={proptietaryText}
               social={social}
               stays={stays}
               urlname={urlname}
               webSite={webSite}
               location={geoLocation}
               path={fullPathName}           
                />)
            }
            {
              commercial &&
              commercial.map(({commercialName, images, urlCommercialName, commercialType},index)=> 

                
                  <ShowCommercial
                    key={index}
                    title={commercialName}
                    picture = {images}
                    link={urlCommercialName}
                    subTitle={commercialType}
                  >


                  </ShowCommercial>
                
              )

            }

        
  
      </div>
  );
}

export default Propietary;