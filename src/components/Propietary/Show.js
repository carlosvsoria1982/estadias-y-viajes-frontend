import React from 'react';
import { Col, Row, Container, Figure } from 'react-bootstrap'
import '../Propietary/Style.css';
import { Facebook, Instagram, WhatsApp, AlternateEmail, RecordVoiceOver} from '@material-ui/icons'
import { green , blue} from '@material-ui/core/colors';
import { IMAGE_URL, BASE_URL} from '../../Enviroment/enviroment'
import {Link } from 'react-router-dom'
function Show({
    active,
    bio,
    email,
    experiences,
    images,
    languages,
    phoneNumber,
    propietaryName,
    proptietaryText,
    social,
    stays,
    updatedAt,
    urlname,
    webSite,
    path
}) {
    const baseURL = `${BASE_URL}${path}`
    const WhatsAppUrl = `https://api.whatsapp.com/send?phone=${phoneNumber}&text=hola ${propietaryName}, te escribo por tu aviso en ${baseURL}`
    const emailPropietary =`mailto:${email}`
    // console.log({baseURL})
    const socialMediaAviables=['facebook', 'instagram']
    // var array = []
    // console.log(socialMediaAviables.length)
    const facebookUrl = social[0][socialMediaAviables[0]]
    const instagramUrl = social[0][socialMediaAviables[1]]
    const isFacebook = socialMediaAviables[0]
    const isInstagram = socialMediaAviables[1]
    // console.log(facebookUrl)
    // console.log(instagramUrl)


    
    // for (let index = 0; (socialMediaAviables.length-1); index++) {
        
    //     var obj = Object.keys(social[index]);
    //     console.log(obj)
    //     var newUrl = social[index][socialMediaAviables[index]]
        
    //     // array.push(newUrl)
    //     console.log(newUrl)
    // }
    
  
    // console.log(array)
  


    return (

        <Container>

            <Row>
                <Col lg={5}>
                    <Figure >
                        <Figure.Image roundedCircle
                            width={171}
                            height={180}
                            alt="171x180"
                            src={IMAGE_URL+images[0].imageUrl}
                        />
                    </Figure>

                </Col>
                <Col lg={7}>
                    <h3 className="texto-principal">{propietaryName}</h3>
                    <h5 className="texto-principal">{proptietaryText}</h5>
                    <Row>
                        <Col >
                         <h6 className="texto-principal"><RecordVoiceOver className="icono-aumentado"/>&nbsp;&nbsp;&nbsp;{languages}</h6>
                        </Col>


                    </Row>
                    <Row>
                        <Col>
                            <h6 className="alineacion-izq">
                            <a href={emailPropietary}><AlternateEmail style={{ color: blue[500] }} className="icono-aumentado"/>&nbsp;&nbsp;&nbsp;{email}</a>
                            </h6>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h6 className="alineacion-izq">
                            <Link to={{ pathname: WhatsAppUrl}} target="_blank">
                            <WhatsApp style={{ color: green[500] }} className="icono-aumentado" />
                            &nbsp;&nbsp;&nbsp;{phoneNumber}</Link>

                            </h6>
                        </Col>
                    </Row>

                </Col>
            </Row>
            
                <Row>
                    <Col lg={4}>
                        <Row>
                             {
                               
                                    isFacebook === 'facebook' &&
                                        
                                            
                                                <Col>
                                                <Link to={{ pathname: facebookUrl}} target="_blank">
                                                    <Facebook className="icono-aumentado" color="primary" />
                                                </Link>        
                                                </Col>
                                    
                            }
                            {
                            isInstagram === 'instagram' &&
                                        
                                            
                                        <Col>
                                        <Link to={{ pathname: instagramUrl}} target="_blank">
                                            <Instagram className="icono-aumentado" color="secondary" />
                                        </Link>        
                                        </Col>
                            }
                        </Row>
                       
                    </Col>
                    
                </Row>
                <Row>
                <Col lg={2}>
                    <h1></h1>
                    <h3 className="texto">Bio</h3>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h6 className="text-align: right;">{bio}</h6>
                </Col>
            </Row>


        </Container>
    )
};




export default Show;