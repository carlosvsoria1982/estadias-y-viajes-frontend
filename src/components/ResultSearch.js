import React from "react";
import { Card } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../App.css'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faCoffee } from '@fortawesome/free-solid-svg-icons'
// import { faBed, faTruckMonster, faRoute, faMapSigns, faConciergeBell, faWalking } from '@fortawesome/free-solid-svg-icons'
import { IMAGE_URL} from '../Enviroment/enviroment'
import { BrowserRouter as Router, Link } from 'react-router-dom'
function ResultSearch({ imageMainText, images, urlName, imageSubText, id, staysCount, experiencesCount, placesCount }) {
    return (
        <Card style={{ color: "#000", width: 250, height: 300, margin: 10 }}>
            <Link to={{ pathname: `/province/${urlName}`, state: { id: id } }}>
            <Card.Header>{imageMainText}</Card.Header>
            <Card.Img variant="bottom" src={IMAGE_URL+images.imageUrl} />
            </Link>

            
            <Card.Body>
                {/* <Link to={{ pathname: `/province/${urlName}`, state: { id: id } }}>
                    <Card.Title>{imageMainText}</Card.Title>
                </Link> */}
                <Card.Subtitle variant="bottom">
                    {imageSubText}
                </Card.Subtitle>
                {/* <Card.Footer className="text-muted text-footer">2 days ago</Card.Footer> */}

                {/* <Button style={{ position: 'absolute', bottom: 0, left: 0, }} href={`localhost:3000//province/${courses.urlName}`} variant="primary" block>conocer más de {courses.provinceName} ...</Button> */}
            </Card.Body>
            {/* <Card.Footer>
                    <Row >
                        <Col>
                            
                            <a href="https://bit.ly/39WMR6q" target="_blank" title="hospedajes"> <FontAwesomeIcon icon={faBed} />({staysCount})</a>
                        </Col>
                        <Col>
                            <a href="/google.com" title="experiencias"> <FontAwesomeIcon icon={faWalking} />({experiencesCount})</a>
                        </Col>
                        <Col>
                            <a href="/google.com" title="lugares"> <FontAwesomeIcon icon={faMapSigns} />x{placesCount}</a>
                        </Col>
                    </Row>
                    </Card.Footer> */}
        </Card>

    )
};

export default ResultSearch