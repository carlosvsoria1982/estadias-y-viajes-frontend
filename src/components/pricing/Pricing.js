import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckSquare, faWindowClose } from '@fortawesome/free-solid-svg-icons'
import { Col, Row, Container, Table } from 'react-bootstrap'

function pricing() {

    return (
        <div>
            {/* <FontAwesomeIcon icon={faCheckSquare} />
                <FontAwesomeIcon icon={faWindowClose} /> */}
            <Container fluid>
                <Row>
                    <Col>Aqué encontrarás los detalles de cada plan de publicación</Col>
                </Row>
            </Container>
<br/>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th></th>
                        <th>Free</th>
                        <th>Intermedio</th>
                        <th>Avanzado</th>


                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Perfil</td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>1 Estadías</td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>1 experiencia</td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>Estadías y experiencias Ilimitadas</td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>Aviso Destacado en localidad</td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>Aviso Destacado en Provincia</td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>Imágenes ilimitadas</td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>Íconos increibles</td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>Soporte y Asesoramiento</td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faWindowClose} /></td>
                        <td><FontAwesomeIcon icon={faCheckSquare} /></td>
                    </tr>
                    <tr>
                        <td>Precios</td>
                        <td>Gratis!</td>
                        <td>$400</td>
                        <td>$700</td>
                    </tr>
                    <tr>
                        <td>Ejemplos</td>
                        <td>link to basic</td>
                        <td>link to intermedio</td>
                        <td>link to avanzado</td>
                    </tr>

                </tbody>
            </Table>
            <div>

            </div>
        </div>
    );
}

export default pricing;