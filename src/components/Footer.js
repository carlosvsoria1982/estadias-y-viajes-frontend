import React from 'react'
import { Col, Row, Container } from 'react-bootstrap'
import { Email, Phone, WhatsApp} from '@material-ui/icons'
import { green, blue } from '@material-ui/core/colors';
import { Link } from 'react-router-dom'

function Footer() {
  const phoneNumber = "+549116543-4529"
  const WhatsAppUrl = `https://api.whatsapp.com/send?phone=${phoneNumber}&text=Hola! Te escribo para conocer más de estadiasyviajes.com: `
  return (
    <footer >
      <Container lg={6} >
        <div >
          <Row className="text-footer">

            <Col>
              <p>CONTACTO</p>
              <u>
              <li> <h5> <a href="mailto://info@estadiasyviajes.com" style={{ color: 'white' }}><Email /> info@estadiasyviajes.com</a></h5></li>
              <li>
              <Link to={{ pathname: WhatsAppUrl}} target="_blank">
                  <h5  style={{ color: 'white' }}>
                            <WhatsApp style={{ color: green[500] }} />
                            &nbsp;Enviar mensaje  
                                  
                  </h5>
              </Link>
              </li>
              
              <li> <h5><a href="callto://+5492646702524" style={{ color: 'white' }}><Phone style={{ color: blue[500] }} /> {phoneNumber}</a></h5></li>

            </u>
            </Col>
            <Col>
            <p>+Info</p>
            <u>
              <Link to="/about">
                <li>Acerca de</li>
              </Link>
              <Link to="/terms">
                <li>Términos y Condiciones</li>
              </Link> 
              <Link to="/pricing">
                <li>Precios</li>
              </Link>
            </u>
            </Col>

          </Row>
          <hr></hr>
          <Row sm={3}>
            
            



          </Row>


        </div>
        <div className="footer-copyright text-center py-3">© 2021 Copyright:
          <a href="https://estadiasyviajes.com"> estadiasyviajes.com</a>
        </div>
      </Container>
    </footer >
  )

}
export default Footer;