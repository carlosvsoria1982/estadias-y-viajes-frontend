let BASE_URL = ""
let IMAGE_URL = ""
let ENV = ""
if (process.env.REACT_APP_ENV === "development") {
    BASE_URL = "http://localhost:8081"
    IMAGE_URL = "http://localhost:8081/public/"
    ENV = "development"
}
if (process.env.REACT_APP_ENV === "production") {
    BASE_URL = "https://estadiasyviajes.com"
    IMAGE_URL = "https://estadiasyviajes.com/static/media/imgs/"
    // BASE_URL = "http://localhost:8081"
    // IMAGE_URL = "http://localhost:8081/public/"
    ENV = "production"
}

export { BASE_URL, IMAGE_URL, ENV}