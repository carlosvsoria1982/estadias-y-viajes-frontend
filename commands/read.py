from pymongo import MongoClient
# pprint library is used to make the output look more pretty
#from pprint import pprint
from bson.objectid import ObjectId
#import os
#import sys
import json
from bson import BSON
from bson import json_util
# connect to MongoDB, change the << localhost:27017 >> to reflect your own connection string
client = MongoClient("mongodb://localhost:27017/")
# Issue the serverStatus command and print the results
# serverStatusResult=db.command("serverStatus")
print('server conectado')
print(client.list_database_names())
mndb = client ["api-rest"]
collist = mndb.list_collection_names()
print(collist)
# if "provinces" in collist:
#   print("The collection provinces exits.")
mycol = mndb['provinces']
#for x in mycol.find():
#  print(x)
for x in mycol.find({"_id": ObjectId('5f8b2e539bd5a93cfc992d81')}):
    #print(x)
    j = json.dumps(x, sort_keys=True, indent=4, default=json_util.default)
    print(j)